# Pose-estimation

Human Pose Estimation is defined as the problem of localization of human joints (also known as keypoints - elbows,knees, wrists, etc) in images or videos.

<img src="/image folder/pose_detection2.png" width="700" height="350">


In addition to tracking human movement and activity, pose estimation opens up applications in a range of areas, such as:
* Augmented reality
* Animation
* Gaming
* Robotics 

A very popular Deep Learning app HomeCourt uses Pose Estimation to analyse Basketball player movements.

### Why is it hard?
Strong articulations, a person's hand covering their face,small and barely visible joints, occlusions, clothing, and lighting changes make this a difficult problem.
<img src="/image folder/psoe-estimation_challengespng.png" width="700" height="350">

