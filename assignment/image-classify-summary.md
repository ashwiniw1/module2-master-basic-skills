# Image Classification
Image Classification Classification System consists of database that contains predefined patterns that compares with detected object to classify in to proper category.

<img src="/image folder/imageclassification.jpg" width="450" height="400">



* **Digital Data:** An image is captured by using digital camera or any mobile phone camera.

<img src="/image folder/Digital_data.png" width="150" height="150">

* **Pre-processing:** Improvement of the image data that surpresses the unwanted distortions and enhances the important image features.

<img src="/image folder/Pre_Processing.png" width="550" height="350">


* **Feature extraction:** It is the process by which certain features of interest within an image sample are detected and represented for further processing.


* **Selection of training data:** Selection of the particular attribute which best describes the pattern. 

<img src="/image folder/Selection_of_trainingdata.png" width="550" height="350">

* **Decision and Classification:** Categorizes detected objects into predefined classes by using suitable method that compares the image patterns with the target patterns. Classification refers to the task of extracting information classes from a multiband raster image.

<img src="/image folder/Decision&classifiation.png" width="900" height="350">

*  **Accuracy assessment:** An accuracy assessment is realized to identify possible sources of errors and as an indicator used in comparisons.