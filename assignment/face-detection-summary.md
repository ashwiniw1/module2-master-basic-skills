# FACE Detection
Face detection is an artificial intelligence (AI) based computer technology used to find and identify human faces in digital images. This technology can be applied to various fields including security, biometrics, law enforcement, personal safety -- to provide surveillance and tracking of people in real time.

<img src="/image folder/facedetection1.png" width="300" height="200">


### **Face detection *V/S* Face Recognition**

Face detection just means that a system is able to identify that there is a human face present in an image or video. Face recognition can confirm identity. It is therefore used to control access to sensitive areas.


## Face Detection methods:
<img src="/image folder/face detection2.png" width="700" height="300">

* **Knowledge-Based**

Its a top down approach Based on rules set by the human knowledge to detect the faces i.e two eyes that are symmetric to each other,nose and a mouth

* **Feature-Based**

It locates faces by extracting structural features of the face(for example, image edges, corners, and other structures well localized in two dimensions) and tracks these as they move from frame to frame.

* **Template Matching**

A process for face detection, which involves multi-resolution template matching,region clustering and color segmentation, works with high accuracy, and gives good statistical results with training images.

* **Appearance-Based**

The object recognition features are chosen to be the pixel intensity values.This method also used in feature extraction for face recognition.


### Algorithm which are used for face detection
* Principle Component Analysis (PCA) 
* Eigenface based algorithm 
* Haar Cascade classifier
* CNNs
* Fisherface based algorithm


The most common application where face detection and recognition algorithms are used is the real time attendance system.
It is a process of recognizing the student/employees face for taking attendance by using face biometrics.


<img src="/image folder/attendance.jpg" width="700" height="300">